package com.universe.datastructure.queue;

import java.util.Scanner;

/**
 * @Description: 环形队列
 * @Author: chenhuan
 * @Date: 15:31 2019/12/15
 */
public class CircleArrayQueueDemo {
    public static void main(String[] args) {
        CircleArray queue = new CircleArray(3);
        String key = "";
        Scanner scanner = new Scanner(System.in);
        boolean loop = true;
        while (loop) {
            System.out.println("show:显示队列");
            System.out.println("exit:退出程序");
            System.out.println("add:添加数据");
            System.out.println("get:取出数据");
            System.out.println("head:查看队头");
            key = scanner.next();
            switch (key) {
                case "show":
                    queue.showQueue();
                    break;
                case "add":
                    System.out.println("请输入您要添加的数字：");
                    int value = scanner.nextInt();
                    queue.addQueue(value);
                    break;
                case "get":
                    try {
                        System.out.println("取出的数据是：" + queue.getQueue());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "head":
                    try {
                        System.out.println("对头数据是：" + queue.headQueue());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "exit":
                    scanner.close();
                    loop = false;
                    break;
                default:
                    break;
            }
        }
    }
}

class CircleArray {
    private int maxSize;
    private int[] arr;
    //front 变量的含义做一个调整： front 就指向队列的第一个元素, 也就是说 arr[front] 就是队列的第一个元素
    //front 的初始值 = 0
    private int front;
    //rear 变量的含义做一个调整：rear 指向队列的最后一个元素的后一个位置. 因为希望空出一个空间做为约定.
    //rear 的初始值 = 0
    private int rear;

    public CircleArray(int maxSize) {
        this.maxSize = maxSize;
        arr = new int[maxSize];
    }

    public void addQueue(int value) {
        if (isFull()) {
            System.out.println("队列已满，不能再加了");
            return;
        }
        arr[rear] = value;
        rear = (rear + 1) % maxSize;
    }

    public int getQueue() {
        if (isNull()) {
            throw new RuntimeException("队列已空，不能再取了");
        }
        int value = arr[front];
        front = (front + 1) % maxSize;
        return value;
    }

    public void showQueue() {
        if (isNull()) {
            System.out.println("队列为空");
            return;
        }
        for (int i = front; i < front +size(); i++) {
            System.out.printf("arr[%d]=%d\n", i % maxSize, arr[i % maxSize]);
        }
    }

    private int size() {
        return (rear + maxSize - front) % maxSize;
    }


    public int headQueue() {
        if (isNull()) {
            throw new RuntimeException("队列已空");
        }
        return arr[front];
    }

    private boolean isNull() {
        return rear == front;
    }


    private boolean isFull() {
        return (rear + 1) % maxSize == front;
    }
}