package com.universe.datastructure.huffmantree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author：ghostbamboo
 * @Description： 哈夫曼树
 * @Theory： 1) 给定 n 个权值作为 n 个叶子结点，构造一棵二叉树，若该树的带权路径长度(wpl)达到最小，称这样的二叉树为
 * 最优二叉树，也称为哈夫曼树(Huffman Tree), 还有的书翻译为霍夫曼树。
 * 2) 哈夫曼树是带权路径长度最短的树，权值较大的结点离根较近
 * 相关概念:
 * 1) 路径和路径长度：在一棵树中，从一个结点往下可以达到的孩子或孙子结点之间的通路，称为路径。通路中分
 * 支的数目称为路径长度。若规定根结点的层数为 1，则从根结点到第 L 层结点的路径长度为 L-1
 * 2) 结点的权及带权路径长度：若将树中结点赋给一个有着某种含义的数值，则这个数值称为该结点的权。结 点
 * 的带权路径长度为：从根结点到该结点之间的路径长度与该结点的权的乘积
 * 3) 树的带权路径长度：树的带权路径长度规定为所有叶子结点的带权路径长度之和，记为 WPL(weighted path
 * length) ,权值越大的结点离根结点越近的二叉树才是最优二叉树。
 * 4) WPL 最小的就是哈夫曼树
 * @Date： 17:29 2020/1/7
 */
public class HuffmanTree {

    public static void main(String[] args) {
        int arr[] = {13, 7, 8, 3, 29, 6, 1};
        Node root = createHuffmanTree(arr);
        preOrder(root);

    }


    public static void preOrder(Node root) {
        if (root != null) {
            root.preOrder();
        } else {
            System.out.println("树为空无法遍历");
        }
    }

    public static Node createHuffmanTree(int[] arr) {
        //---------------------------- 1 ----------------------------
        //遍历数组,放入ArrayList中
        List<Node> nodes = new ArrayList<>();
        for (int value : arr) {
            nodes.add(new Node(value));
        }

        //---------------------------- 2 ----------------------------
        //循环构建哈夫曼树,直至nodes数组中仅剩根节点的权值时构建成功
        while (nodes.size() > 1) {

            //从小到大排序
            Collections.sort(nodes);

            //每次取出根节点权值最小的二叉树
            //(1) 取出权值最小的结点（二叉树）
            Node leftNode = nodes.get(0);
            //(2) 取出权值第二小的结点（二叉树）
            Node rightNode = nodes.get(1);

            //(3)构建一颗新的二叉树
            Node parent = new Node(leftNode.value + rightNode.value);
            parent.left = leftNode;
            parent.right = rightNode;

            //(4)从ArrayList删除处理过的二叉树
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            //(5)将parent加入到nodes
            nodes.add(parent);
        }
        //---------------------------- 3 ----------------------------
        //返回根节点
        return nodes.get(0);

    }

}


class Node implements Comparable<Node> {

    int value; // 结点权值
    char c; //字符
    Node left; // 指向左子结点
    Node right; // 指向右子结点

    public Node(int value) {
        this.value = value;
    }

    public void preOrder() {
        System.out.println(this);
        if (this.left != null) {
            this.left.preOrder();
        }
        if (this.right != null) {
            this.right.preOrder();
        }
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    @Override
    public int compareTo(Node o) {
        //从小到大排序
        return this.value - o.value;
    }
}

