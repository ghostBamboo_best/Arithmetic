package com.universe.datastructure.hashtab;

import com.sun.glass.ui.Size;

import java.security.Key;
import java.util.Scanner;

/**
 * @Author：ghostbamboo
 * @Description： 散列表（Hash table，也叫哈希表），是根据关键码值(Key value)而直接进行访问的数据结构。
 * 也就是说，它通 过把关键码值映射到表中一个位置来访问记录，以加快查找的速度。这个映射函数叫做散列函数，
 * 存放记录的数组 叫做散列表。
 * @Date： 14:38 2020/1/6
 */
public class HashTabDemo {
    public static void main(String[] args) {

        HashTab hashTab = new HashTab(5);

        Scanner scanner = new Scanner(System.in);
        String key = "";
        while (true) {
            System.out.println("add:  添加雇员");
            System.out.println("list: 显示雇员");
            System.out.println("find: 查找雇员");
            System.out.println("exit: 退出系统");

            key = scanner.next();
            switch (key) {
                case "add":
                    System.out.println("输入id:");
                    int id = scanner.nextInt();
                    System.out.println("输入名字:");
                    String name = scanner.next();
                    //创建 雇员
                    Emp emp = new Emp(id, name);
                    hashTab.add(emp);
                    break;
                case "list":
                    hashTab.list();
                    break;
                case "find":
                    System.out.println("请输入要查找的id");
                    id = scanner.nextInt();
                    hashTab.findEmpById(id);
                    break;
                case "exit":
                    scanner.close();
                    System.exit(0);
                default:
                    break;
            }
        }
    }
}

//创建HashTab 管理多条链表
class HashTab {
    private EmpLinkedList[] empLinkedListArray;
    //数组大小
    private int maxSize;

    public HashTab(int maxSize) {
        this.empLinkedListArray = new EmpLinkedList[maxSize];
        this.maxSize = maxSize;
        //切记需要循环初始化每条链表
        for (int i = 0; i < maxSize; i++) {
            empLinkedListArray[i] = new EmpLinkedList();
        }
    }

    /**
     * 添加雇员
     */
    public void add(Emp emp) {
        //根据员工id得知加入到哪个链表
        int i = hashFun(emp.id);
        empLinkedListArray[i].add(emp);
    }

    /**
     * 遍历所有雇员
     */
    public void list() {
        for (int i = 0; i < maxSize; i++) {
            empLinkedListArray[i].list(i);
        }
    }

    /**
     * 根据雇员id查询雇员信息
     *
     * @param id
     */
    public void findEmpById(int id) {
        int i = hashFun(id);
        Emp emp = empLinkedListArray[i].findEmpById(id);
        if (emp != null) {//找到
            System.out.printf("在第%d条链表中找到 雇员 id = %d\n", (i + 1), id);
        } else {
            System.out.println("在哈希表中，没有找到该雇员~");
        }
    }

    /**
     * 根据员工id计算存储到哪个链表
     *
     * @param no 员工id
     * @return
     */
    private int hashFun(int no) {
        return no % maxSize;
    }

}

//雇员
class Emp {
    public int id;
    public String name;
    public Emp next;

    public Emp(int id, String name) {
        this.id = id;
        this.name = name;
    }
}

//链表
class EmpLinkedList {
    //头指针
    private Emp head;

    public void add(Emp emp) {
        //优先处理头结点
        if (head == null) {
            head = emp;
            return;
        }

        //如果不是头结点,则默认添加到最后
        Emp curEmp = head;
        while (true) {
            if (curEmp.next != null) {
                curEmp = curEmp.next;
            } else {
                curEmp.next = emp;
                break;
            }
        }
    }

    //遍历链表的雇员信息
    public void list(int no) {
        if (head == null) { //说明链表为空
            System.out.println("第 " + (no + 1) + " 链表为空");
            return;
        }
        System.out.print("第 " + (no + 1) + " 链表的信息为");
        Emp curEmp = head; //辅助指针
        while (true) {
            System.out.printf(" => id=%d name=%s\t", curEmp.id, curEmp.name);
            if (curEmp.next == null) {//说明curEmp已经是最后结点
                break;
            }
            curEmp = curEmp.next; //后移，遍历
        }
        System.out.println();
    }

    //根据id查找雇员
    //如果查找到，就返回Emp, 如果没有找到，就返回null
    public Emp findEmpById(int id) {
        //判断链表是否为空
        if (head == null) {
            System.out.println("链表为空");
            return null;
        }
        //辅助指针
        Emp curEmp = head;
        while (true) {
            if (curEmp.id == id) {//找到
                break;//这时curEmp就指向要查找的雇员
            }
            //退出
            if (curEmp.next == null) {//说明遍历当前链表没有找到该雇员
                curEmp = null;
                break;
            }
            curEmp = curEmp.next;//以后
        }

        return curEmp;
    }

}