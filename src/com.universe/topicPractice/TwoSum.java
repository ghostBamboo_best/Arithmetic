package com.universe.topicPractice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 题目：
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * 示例：
 * Given nums = [2, 7, 11, 15], target = 9,
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 */

class TwoSum {
    public static void main(String[] args) {
        int[] nums = {1, 2, 5, 7, 8, 3, 4};
        int[] sum = new TwoSum().twoSum(nums, 7);
        System.out.println(Arrays.toString(sum));
    }

    //自己提供的解法，时间复杂度为O(n^2)，空间复杂度为O(n)，考虑到数组中存在多组数字相加得到target的情况
    public int[] twoSum(int[] nums, int target) {
        int index = 0;
        int[] temp = new int[0];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    temp = Arrays.copyOf(temp, temp.length + 2);
                    temp[index] = i;
                    temp[index + 1] = j;
                    index += 2;
                }
            }
        }
        return temp;
    }

    //标准解法，时间复杂度为O(n^2)，空间复杂度为O(1)
    public int[] twoSumOri(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[j] == target - nums[i]) {
                    return new int[] { i, j };
                }
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }

    //使用哈希对第一种解法进行改进，时间复杂度为O(n)，空间复杂度为O(n)
    public int[] twoSumPro(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
        }
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement) && map.get(complement) != i) {
                return new int[] { i, map.get(complement) };
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }

    //一次通过hash的方式对上一种解法进行优化，时间复杂度为O(n), 空间复杂度为O(n)
    public int[] twoSumPlus(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement)) {
                return new int[] { map.get(complement), i };
            }
            map.put(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}