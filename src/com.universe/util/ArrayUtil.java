package com.universe.util;

import java.util.Arrays;

/**
 * @Author：ghostbamboo
 * @Description： 数组比较的工具类
 * @Date： 12:38 2020/1/5
 */
public class ArrayUtil {


    // 系统排序
    public static void systemSort(int... array) {
        Arrays.sort(array);
    }

    // 生成随机数组
    public static int[] generateArray(int maxSize, int maxValue) {
        int[] array = new int[(int) (Math.random() * (maxSize + 1))];
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = (int) (Math.random() * (maxValue + 1));
        }
        return array;
    }


    //数组复制
    public static int[] copyArray(int... array) {
        return array.clone();
    }

    // 数组比对
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }
}
