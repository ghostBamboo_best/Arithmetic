package com.universe.arithmetic.search;

/**
 * @Author：ghostbamboo
 * @Description： 线性查找算法, 即暴力查找
 * @Date： 10:07 2020/1/6
 */
public class LinearSearch {


    public static int linearSearch(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        int[] arr = {1, 4, 8, 23, 34, 14, 56, 24};
        int value = 10;
        int res = linearSearch(arr, value);
        if (res != -1) {
            System.out.printf("找到 %d 对应的下标为 %d ", value, res);
        } else {
            System.out.println(value + " 没有找到");
        }
    }
}
