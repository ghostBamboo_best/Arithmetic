package com.universe.arithmetic.baseSort;

import java.util.Arrays;

/**
 * @Author：chenhuan
 * @Description： 选择排序
 * @Theory： 每一次从待排序的数据元素中选出最小（或最大）的一个元素，存放在序列的起始位置，
 * 然后，再从剩余未排序元素中继续寻找最小（大）元素，然后放到已排序序列的末尾。以
 * 此类推，直到全部待排序的数据元素排完。 选择排序是不稳定的排序方法。
 * 平均时间复杂度 O（n^2） 空间复杂度 O（1） 不稳定排序
 * @Date： 10:18 2019/6/13
 */
public class SellectionSort {

    //选择排序
    public static void sellctionSort(int... arr) {
        int temp;
        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j <= arr.length - 1; j++) {
                if (arr[j] < arr[index]) {
                    index = j;
                }
            }
            if (index != i) {
                temp = arr[i];
                arr[i] = arr[index];
                arr[index] = temp;
            }
        }
    }

    // 系统排序
    public static void comparator(int... array) {
        Arrays.sort(array);
    }

    // 生成随机数组
    public static int[] generateArray(int maxSize, int maxValue) {
        int[] array = new int[(int) (Math.random() * (maxSize + 1))];
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = (int) (Math.random() * (maxValue + 1));
        }
        return array;
    }


    //数组复制
    public static int[] copyArray(int... array) {
        return array.clone();
    }

    // 数组比对
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {

        //测试单数组排序
        //int[] arr = {1, 5, 6, 4, 3, 12};
        //sellctionSort(arr);
        //for (int i : arr) {
        //    System.out.print(i + " ");
        //}

        int testSize = 1000;
        int maxSize = 1000;
        int maxValue = 9999;
        boolean success = true;
        for (int i = 0; i < testSize; i++) {
            int[] array1 = generateArray(maxSize, maxValue);
            int[] array2 = copyArray(array1);
            sellctionSort(array1);
            comparator(array2);
            boolean b = isEqual(array1, array2);
            if (!b) {
                success = false;
                break;
            }
        }
        System.out.println(success ? "Nice! 成功" : "What a pity! Failed!!!");
    }
}
