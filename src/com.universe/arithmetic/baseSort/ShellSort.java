package com.universe.arithmetic.baseSort;

import com.universe.util.ArrayUtil;

/**
 * @Author：ghostbamboo
 * @Description： 希尔排序
 * @Theory： 插入排序问题：当需要插入的数值较小时，需要移位的次数就增加，影响效率
 * 希尔排序也是一种插入排序，希尔排序是把记录按下标的一定增量分组，对每组使用直接
 * 插入排序算法排序；随着增量逐渐减少，每组包含的关键词越来越多，当增量减至 1 时，
 * 整个文件恰被分成一组，算法便终止。
 * 平均时间复杂度 O（nlogn） 空间复杂度 O（1）
 * @Date： 16:21 2019/12/19
 */
public class ShellSort {

    /**
     * 希尔排序（交换法）
     */
    public static void shellSortByExchange(int... arr) {
        //根据步长确定执行轮数
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            //根据步长遍历每组元素
            for (int i = gap; i < arr.length; i++) {
                for (int j = i - gap; j >= 0; j -= gap) {
                    if (arr[j] > arr[j + gap]) {
                        int temp = arr[j];
                        arr[j] = arr[j + gap];
                        arr[j + gap] = temp;
                    }
                }
            }
        }
    }

    /**
     * 希尔排序（插入法）
     */
    public static void shellSortByInsert(int... arr) {
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < arr.length; i++) {
                int insertIndex = i;
                int insertValue = arr[insertIndex];
                while (insertIndex - gap >= 0 && arr[insertIndex - gap] > insertValue) {
                    arr[insertIndex] = arr[insertIndex - gap];
                    insertIndex -= gap;
                }
                arr[insertIndex] = insertValue;
            }
        }
    }


    public static void main(String[] args) {
        int[] arr = {8, 9, 1, 7, 2, 3, 5, 4, 6, 0};
        int[] copy = ArrayUtil.copyArray(arr);

        ArrayUtil.systemSort(copy);
        //shellSortByExchange(arr);
        shellSortByInsert(arr);
        boolean success = ArrayUtil.isEqual(arr, copy);
        System.out.println(success ? "Nice! 排序成功" : "What a pity! Failed!!!");
        for (int i : arr) {
            System.out.print(i + " ");
        }

    }
}
