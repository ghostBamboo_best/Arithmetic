package com.universe.arithmetic.baseSort;

import java.util.Arrays;

/**
 * @Author：chenhuan
 * @Description： 冒泡排序
 * @Theory： 1.比较相邻的元素。如果第一个比第二个大，就交换他们两个。
 * 2.对每一对相邻元素做同样的工作，从开始第一对到结尾的最后一对。在这一点，最后的元素应该会是最大的数。
 * 3.针对所有的元素重复以上的步骤，除了最后一个。
 * 4.持续每次对越来越少的元素重复上面的步骤，直到没有任何一对数字需要比较。
 * 平均时间复杂度 O（n^2） 空间复杂度 O（1） 稳定排序
 * @Date： 14:12 2019/6/6
 */
public class BubbleSort {

    public static int count = 0;

    // 冒泡排序
    public static void bubbleSort(int... array) {
        boolean flag = false;
        int temp;
        if (array == null || array.length < 2) {
            return;
        }
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                    flag = true;
                }
            }
            if (flag) {
                flag = false;
            } else {
                break;
            }
            count++;
        }
    }

    // 系统排序
    public static void comparator(int... array) {
        Arrays.sort(array);
    }

    // 生成随机数组
    public static int[] generateArray(int maxSize, int maxValue) {
        int[] array = new int[(int) (Math.random() * (maxSize + 1))];
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = (int) (Math.random() * (maxValue + 1));
        }
        return array;
    }

    //数组复制
    public static int[] copyArray(int... array) {
        return array.clone();
    }

    // 数组比对
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    // 测试
    public static void main(String[] args) {

        //测试单数组排序
        int[] arr = {1, 5, 6, 4, 3, 12};
        bubbleSort(arr);
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.printf("\n共执行%d轮", count);

        //测试多数组排序成功否
        //int testTime = 10;
        //int maxSize = 9999;
        //int maxValue = 9999;
        //boolean success = true;
        //for (int i = 0; i < testTime; i++) {
        //    int[] array1 = generateArray(maxSize, maxValue);
        //    int[] array2 = copyArray(array1);
        //    bubbleSort(array1);
        //    comparator(array2);
        //    boolean b = isEqual(array1, array2);
        //    if (!b) {
        //        success = false;
        //        break;
        //    }
        //}
        //System.out.println(success ? "排序成功！Great！" : "排序失败，下次加油！");


    }


}
