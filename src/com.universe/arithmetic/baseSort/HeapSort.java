package com.universe.arithmetic.baseSort;

import com.universe.util.ArrayUtil;

/**
 * @Author：ghostbamboo
 * @Description： 堆排序
 * @Theory： 堆排序说明:
 * 1) 堆排序是利用堆这种数据结构而设计的一种排序算法，堆排序是一种选择排序，
 * 它的最坏，最好，平均时间复 杂度均为 O(nlogn)，它也是不稳定排序。
 * 2) 堆是具有以下性质的完全二叉树：每个结点的值都大于或等于其左右孩子结点
 * 的值，称为大顶堆, 注意 : 没有 要求结点的左孩子的值和右孩子的值的大小关系。
 * 3) 每个结点的值都小于或等于其左右孩子结点的值，称为小顶堆。
 * 4）大顶堆特点：arr[i] >= arr[2n + 1] && arr[i] >= arr[2n + 2]
 * 小顶堆特点：arr[i] <= arr[2n + 1] && arr[i] <= arr[2n + 2]
 * 5) 一般升序采用大顶堆，降序采用小顶堆
 * <p>
 * 堆排序的基本思想是：
 * 1) 将待排序序列构造成一个大顶堆
 * 2) 此时，整个序列的最大值就是堆顶的根节点。
 * 3) 将其与末尾元素进行交换，此时末尾就为最大值。
 * 4) 然后将剩余 n-1 个元素重新构造成一个堆，这样会得到 n 个元素的次小值。如此反复执行，便能得到一个有序序列了。
 * @Date： 22:35 2020/1/5
 */
public class HeapSort {


    public static void heapSort(int[] arr) {
        //--------------------------------- 1 -------------------------------
        //将数组调整为一个大顶堆
        for (int i = arr.length / 2 - 1; i >= 0; i--) {
            adjustHeap(arr, i, arr.length);
        }

        //--------------------------------- 2 -------------------------------
        // 将堆顶元素与末尾元素交换，将最大元素"沉"到数组末端;
        //重新调整结构，使其满足堆定义，然后继续交换堆顶元素与当前末尾元素，反复执行调整+交换步骤，直到整个序列有序。
        for (int i = arr.length - 1; i > 0; i--) {
            //交换最大元素到末尾
            int temp = arr[i];
            arr[i] = arr[0];
            arr[0] = temp;
            adjustHeap(arr, 0, i);
        }
    }

    /**
     * 调整堆使其成为一个以i节点为根节点的大顶堆
     *
     * @param arr    要构建大顶堆的数组
     * @param i      要构建以i为根节点的大顶堆
     * @param length 表示对多少个元素继续调整， length 是在逐渐的减少
     */
    public static void adjustHeap(int[] arr, int i, int length) {
        //记录当前节点的值
        int root = arr[i];
        for (int k = 2 * i + 1; k < length; k = 2 * k + 1) {
            //指针指向左子节点与右子节点较大的那个
            if (k + 1 < length && arr[k] < arr[k + 1]) {
                k = k + 1;
            }
            if (arr[k] > root) {
                arr[i] = arr[k];
                i = k; //i 指向 k,继续循环比较
            } else {
                break;
            }
        }
        //当for 循环结束后，以i 为父结点的树的最大值，放在了 最顶(局部)
        arr[i] = root;
    }


    public static void main(String[] args) {
        int[] arr = {8, 9, 1, 7, 2, 3, 5, 4, 6, 0};
        int[] copy = ArrayUtil.copyArray(arr);

        heapSort(arr);
        ArrayUtil.systemSort(copy);
        boolean success = ArrayUtil.isEqual(arr, copy);
        System.out.println(success ? "Nice! 排序成功" : "What a pity! Failed!!!");
        for (int i : arr) {
            System.out.print(i + " ");
        }

    }
}
