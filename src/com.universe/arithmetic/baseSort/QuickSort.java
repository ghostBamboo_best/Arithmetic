package com.universe.arithmetic.baseSort;

import com.universe.util.ArrayUtil;

/**
 * @Author：ghostbamboo
 * @Description： 快速排序
 * @Theory： 快速排序问题：当快速排序（Quicksort）是对冒泡排序的一种改进。基本思想是：
 * 通过一趟排序将要排序的数据分割成独立的两部分，其中一部分的所有数据都比另外一部分的
 * 所有数据都要小，然后再按此方法对这两部分数据分别进行快速排 序，整个排序过程可以递归
 * 进行，以此达到整个数据变成有序序列
 * 平均时间复杂度 O（n * log n） 空间复杂度 O（log n）
 * @Date： 17:41 2019/12/20
 */
public class QuickSort {


    public static void quicksort(int left, int right, int... arr) {
        int l = left;
        int r = right;
        //中间值作为比较的基础值
        int pivot = arr[(left + right) / 2];

        while (l < r) {
            while (arr[l] < pivot) {
                l++;
            }
            while (arr[r] > pivot) {
                r--;
            }
            //左边没找到比pivot更小的数，右边没找到比pivot更大的数，跳出循环，否则交换
            if (l >= r) {
                break;
            }

            int temp = arr[l];
            arr[l] = arr[r];
            arr[r] = temp;


            //如果交换完后，发现这个arr[l] == pivot值 相等 r--， 前移
            if (arr[l] == pivot) {
                r -= 1;
            }
            //如果交换完后，发现这个arr[r] == pivot值 相等 l++， 后移
            if (arr[r] == pivot) {
                l += 1;
            }

        }

        // 如果 l == r, 必须l++, r--, 否则为出现栈溢出
        if (l == r) {
            l += 1;
            r -= 1;
        }

        if (left < r) {
            quicksort(left, r, arr);
        }
        if (right > l) {
            quicksort(l, right, arr);
        }

    }


    public static void main(String[] args) {
        System.out.println("单数组测试………………");
        int[] arr = {-9, 78, 0, 23, -567, 70, -1, 900, 4561};
        int[] copy = ArrayUtil.copyArray(arr);

        quicksort(0, arr.length - 1, arr);
        ArrayUtil.systemSort(copy);
        boolean success = ArrayUtil.isEqual(arr, copy);
        System.out.println(success ? "Nice! 排序成功" : "What a pity! Failed!!!");
        for (int i : arr) {
            System.out.print(i + " ");
        }

        //多数组测试
        System.out.println("\n多数组测试………………");
        for (int i = 0; i < 100; i++) {
            int[] arr1 = ArrayUtil.generateArray(100, 9999);
            int[] arr2 = ArrayUtil.copyArray(arr1);
            quicksort(0, arr1.length - 1, arr1);
            ArrayUtil.systemSort(arr2);
            boolean b = ArrayUtil.isEqual(arr1, arr2);
            if (!b) {
                throw new RuntimeException("排序失败");
            }
        }
        System.out.println("排序成功！");

    }
}
